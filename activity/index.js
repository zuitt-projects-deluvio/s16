let myNum = Number(prompt("Please enter a number"));

console.log(`The number you provided is ${myNum}`);

for (let i = myNum; i >= 0; i--) {
	
	if(i <= 50){
		console.log("The current value is at 50. Terminating the loop.")
		break;
	} else if(i % 10 === 0) {
		console.log(`The number is divisible by 10. Skipping the number.`);
		continue;
	} else if(i % 5 === 0){
		console.log(i);
	}

}